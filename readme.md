# Docker Compose TP3

## Consignes

- Créer un docker-compose avec pour seul service un container basé sur le Dockerfile créé dans le TP WIK-DPS-TP02
- Augmenter le nombre de réplicas à 4 pour ce service
- Modifier le docker-compose pour ajouter un reverse-proxy (nginx), seule le reverse-proxy doit être exposé sur votre hôte sur le port 8080
- Configurer nginx (nginx.conf) pour loadbalancer les requêtes vers le service basé sur votre image
- Modifier le code de votre API pour afficher le hostname dans les logs à chaque requête sur /ping, lancer votre docker-compose.yaml et observer l'effet du l'équilibrage de charge

## Bonus (noté)

- Créer un docker-compose.yaml pour déployer une architecture 3 tiers (app/db/front):
  - un wordpress répliqué au moins 2 fois
  - une base de données mysql
  - une base de données de cache redis
  - un reverse proxy nginx pour servir le wordpress (le seul service exposé)
- Configurer wordpress pour utiliser les différents services
